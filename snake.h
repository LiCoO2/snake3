#include "vector.h"
#ifndef SNAKE
#define SNAKE
/* used as bool and int */
typedef char uint8;

typedef struct ThreadArg {
	int* cancellationToken;
	void* returnV;
} threadArg_t;
void interrupt_handler(int sig);
void* get_dir_loop(void* threadArg);
vector_t parse_dir(char cDir);
vectorp_t move_snake(vector_t dir, int lengthen);
void draw_board(void);
#endif
