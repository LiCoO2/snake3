CFLAGS=-I. -pthread -Wall -Wextra -Wpedantic -std=c99 -march=native -O3 -lm
CFLAGS+=  -Wshadow -Wstrict-prototypes -Wundef -Wpointer-arith -Wcast-align -Wstrict-overflow=5 -Wswitch-default
CFLAGS+= -Wunreachable-code -Wswitch-enum -Wformat=2
PREFIX ?= /usr
BINDIR ?= ${PREFIX}/bin
MANDIR = ${PREFIX}/share/man/

generate:
	$(CC) -o snake3 snake.c vector.c conio.c $(CFLAGS)

install: snake3
	install -d ${DESTDIR}${BINDIR} ${DESTDIR}${MANDIR}/man1
	install snake3 ${DESTDIR}${BINDIR}
	install -m 644 doc/snake3.1 ${DESTDIR}${MANDIR}/man1
clean:
	rm snake3

uninstall:
	-rm -f ${DESTDIR}${BINDIR}/snake3 ${DESTDIR}${MANDIR}/man1/snake3.1
