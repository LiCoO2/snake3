#ifndef VECTOR
#define VECTOR
typedef struct Vector {
	int mx;
	int my;	
} vector_t;

vector_t init_vector(int mx, int my);

typedef struct VectorP {
	vector_t* vectors;
	int length;
} vectorp_t;

vectorp_t init_vectorp(vector_t* vectors, int length);

/* removes a vector from vectorp */
vectorp_t remove_vector(vectorp_t vectorp, int index);
vectorp_t remove_vectors(vectorp_t vectorp, int* indexes, int indexCount);
typedef struct VectorPP {
	vectorp_t* vectorps;
	int length;
} vectorpp_t;

vectorpp_t init_vectorpp(vectorp_t* vectorps, int length);

#endif
