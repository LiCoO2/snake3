#include <stdlib.h>
#include "vector.h"
vector_t init_vector(int mx, int my) {
	vector_t vector;
	vector.mx = mx;
	vector.my = my;
	return vector;
}
vectorp_t init_vectorp(vector_t* vectors, int length) {
	vectorp_t vectorP;
	vectorP.vectors = vectors;
	vectorP.length = length;
	return vectorP;
}
vectorp_t remove_vector(vectorp_t vectorp, int index) {
	vector_t* vectors = malloc((vectorp.length - 1) * sizeof(vector_t));
	for(int i = 0; i < vectorp.length - 1; i++) {
		int correctedIndex = i > index ? i - 1 : i;
		if(i != index) {
			vectors[correctedIndex] = vectorp.vectors[i];
		}
	}
	return init_vectorp(vectors, vectorp.length - 1);
}
vectorp_t remove_vectors(vectorp_t vectorp, int* indexes, int indexCount) {
	vector_t* vectors = malloc((vectorp.length - indexCount) * sizeof(vector_t));
	int correctedIndex = 0;
	for(int i = 0; i < vectorp.length - indexCount; i++) {
		int toRemove = 0;
		for(int j = 0; j < indexCount; j++) {
			if(i == indexes[j]) {
				toRemove = 1;
				break;
			}
		}
		if(!toRemove) {
			vectors[correctedIndex] = vectorp.vectors[i];
			correctedIndex++;
		}
	}
	return init_vectorp(vectors, vectorp.length - indexCount);
}
vectorpp_t init_vectorpp(vectorp_t* vectorps, int length) {
	vectorpp_t vectorPP;
	vectorPP.vectorps = vectorps;
	vectorPP.length = length;
	return vectorPP;
}
